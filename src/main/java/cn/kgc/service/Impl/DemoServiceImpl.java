package cn.kgc.service.Impl;

import cn.kgc.Utils.PageBean;
import cn.kgc.dao.CityDao;
import cn.kgc.dao.Impl.CityDaoImpl;
import cn.kgc.dao.Impl.ToursInfoDaoImpl;
import cn.kgc.dao.ToursInfoDao;
import cn.kgc.domain.City;
import cn.kgc.domain.Condition;
import cn.kgc.domain.ToursInfo;
import cn.kgc.service.DemoService;

import java.util.List;

public class DemoServiceImpl implements DemoService {
    CityDao cityDao =new CityDaoImpl();
    ToursInfoDao toursInfoDao =new ToursInfoDaoImpl();
    @Override
    public List queryToursInfoByCondition(Condition condition) throws Exception {

       List<ToursInfo> toursInfoList= toursInfoDao.queryToursInfoByCondition(condition);
        return toursInfoList;
    }

    @Override
    public boolean addToursInfo(ToursInfo toursInfo) throws Exception {
        Integer integer = toursInfoDao.insertToursInfo(toursInfo);
       if(integer>0){
           return  true;
       }else {
           return false;
       }
    }


    @Override
    public List<City> queryCity() throws Exception {

       List<City> cityList= cityDao.queryCity();
        return cityList;
    }

    @Override
    public boolean deleteToursInfoById(String id) throws Exception {
      int i=  toursInfoDao.deleteToursInfoById(id);

      if(i>0){
          return true;
      }else {
          return false;
      }
    }
    @Override
    public ToursInfo queryToursInfoById(String id) throws Exception {
       ToursInfo toursInfo= toursInfoDao.queryToursInfoById(id);
        return toursInfo;
    }

    @Override
    public boolean updateToursInfo(ToursInfo toursInfo) throws Exception {
          int i =  toursInfoDao.updateToursInfo(toursInfo);
        if(i>0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public PageBean queryToursInfoByPB(Condition condition, Integer p) throws Exception {
          PageBean pb =new PageBean();
          //1，获取总共有多少条记录
         Integer count =toursInfoDao.getCount(condition);

         //2,设置每页显示的条数
          pb.setPageSize(5);

          //3,得出总页数
        pb.setCount(count);

        //4,设置当前页数
        pb.setP(p);
        // 5,分页查询数据  调用dao 的方法

        List<ToursInfo> toursInfoList = toursInfoDao.queryToursInfoByPageBean(condition, pb);

        //6，把分页查询数据放入PageBean
        pb.setData(toursInfoList);

        return pb;
    }


}
