package cn.kgc.service;

import cn.kgc.Utils.PageBean;
import cn.kgc.domain.City;
import cn.kgc.domain.Condition;
import cn.kgc.domain.ToursInfo;

import java.util.List;

public interface DemoService {

    public List<ToursInfo> queryToursInfoByCondition(Condition condition)throws Exception;

    // 插入景点信息

    public boolean addToursInfo(ToursInfo toursInfo)throws Exception;

    public List<City> queryCity() throws Exception;
    //删除景点
    public boolean deleteToursInfoById(String id) throws Exception;

    //查询记录条数
   public PageBean queryToursInfoByPB (Condition condition,Integer p)throws Exception;

   //通过id 查询景点信息
   public ToursInfo queryToursInfoById(String id) throws Exception;

   //更新景点信息
    public boolean updateToursInfo(ToursInfo toursInfo) throws Exception;


}
