package cn.kgc.domain;

import java.io.Serializable;
import java.util.Date;

public class ToursInfo implements Serializable {
   private Integer id;
   private String  introduce;
   private Date     pubTime;
   private Double price;
   private Integer cityId;
   private String  cityName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public ToursInfo() {
    }

    public ToursInfo(Integer id, String introduce, Date pubTime, Double price, Integer cityId) {
        this.id = id;
        this.introduce = introduce;
        this.pubTime = pubTime;
        this.price = price;
        this.cityId = cityId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "ToursInfo{" +
                "id=" + id +
                ", introduce='" + introduce + '\'' +
                ", pubTime=" + pubTime +
                ", price=" + price +
                ", cityId=" + cityId +
                ", cityName='" + cityName + '\'' +
                '}';
    }
}
