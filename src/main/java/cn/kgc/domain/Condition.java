package cn.kgc.domain;

public class Condition {

    private String cityId;
    private String startPrice;
    private String endPrice;
    private String searchName;
    private String cityName;

    public Condition(String cityId, String startPrice, String endPrice, String searchName, String cityName) {
        this.cityId = cityId;
        this.startPrice = startPrice;
        this.endPrice = endPrice;
        this.searchName = searchName;
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Condition() {
    }

    public Condition(String cityId, String startPrice, String endPrice, String searchName) {
        this.cityId = cityId;
        this.startPrice = startPrice;
        this.endPrice = endPrice;
        this.searchName = searchName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(String startPrice) {
        this.startPrice = startPrice;
    }

    public String getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(String endPrice) {
        this.endPrice = endPrice;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
