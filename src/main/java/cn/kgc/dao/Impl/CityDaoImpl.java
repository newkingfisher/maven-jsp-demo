package cn.kgc.dao.Impl;

import cn.kgc.Utils.C3P0Utli;
import cn.kgc.dao.CityDao;
import cn.kgc.domain.City;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.List;

public class CityDaoImpl implements CityDao {
    QueryRunner qr =new QueryRunner(C3P0Utli.getDataSource());
    @Override
    public List<City> queryCity() throws Exception {
        String sql =" select * from city ";
      List<City> cityList= qr.query(sql,new BeanListHandler<City>(City.class));
        return cityList;
    }


}
