package cn.kgc.dao.Impl;
import cn.kgc.Utils.C3P0Utli;
import cn.kgc.Utils.PageBean;
import cn.kgc.dao.ToursInfoDao;
import cn.kgc.domain.Condition;
import cn.kgc.domain.ToursInfo;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.util.ArrayList;
import java.util.List;

public class ToursInfoDaoImpl implements ToursInfoDao {
    QueryRunner qr = new QueryRunner(C3P0Utli.getDataSource());

    @Override
    public List<ToursInfo> queryToursInfoByCondition(Condition condition) throws Exception {
        //创建占位符的集合
         List<Object> parmsList =new ArrayList<>();
        String  cityId =condition.getCityId();
        String startPrice = condition.getStartPrice();
        String  endPrice = condition.getEndPrice();
        String  searchName =condition.getSearchName();

        String sql=" select t.*, c.cityName from toursInfo t left join city c on t.cityId = c.cityId where 1=1 ";

        if(cityId!=null && !cityId.isEmpty()){
            sql+=" and t.cityId = ? ";
            parmsList.add(cityId);
        }
      /*  if(startPrice!=null && !startPrice.isEmpty()){
            sql+=" and t.price >= ? ";
            parmsList.add(startPrice);
        }
        if(endPrice!=null && !endPrice.isEmpty()){
            sql+=" and t.price <= ?";
            parmsList.add(endPrice);
        }
        if(searchName!=null && !searchName.isEmpty()){
            sql+= " and t.introduce like ?";
            parmsList.add("%"+searchName+"%");
        }
        sql+= " order by t.id ";*/
        if(startPrice!=null && !startPrice.isEmpty()){
            sql += " and t.price >= ? ";
            parmsList.add(startPrice);
        }
        if(endPrice!=null && !endPrice.isEmpty()){
            sql+= " and t.price <= ? ";
            parmsList.add(endPrice);
        }
        if(searchName!=null&&!searchName.isEmpty()){
            sql+= " and t.introduce like ? ";
            parmsList.add("%"+searchName+"%");
        }
        sql += " order by t.id ";

        List<ToursInfo> toursInfolist = qr.query(sql, new BeanListHandler<ToursInfo>(ToursInfo.class), parmsList.toArray());

        return toursInfolist;
    }

    @Override
    public Integer insertToursInfo(ToursInfo toursInfo) throws Exception {

        String sql =" insert into toursInfo value (null,?,?,?,? ) ";
        Object[] params =new Object[]{toursInfo.getIntroduce(),toursInfo.getPubTime(),toursInfo.getPrice(),toursInfo.getCityId()};

        int update=qr.update(sql,params);
        return update;
    }



    @Override
    public Integer deleteToursInfoById(String id) throws Exception {

        String sql =" delete from toursInfo where id =? ";
        int del= qr.update(sql,id);
        return del;

    }

    @Override
    public ToursInfo queryToursInfoById(String id) throws Exception {
        String sql =" select * from toursInfo where id =? ";
        ToursInfo toursInfo = qr.query(sql, new BeanHandler<ToursInfo>(ToursInfo.class), id);

        return toursInfo;
    }

    @Override
    public Integer updateToursInfo(ToursInfo toursInfo) throws Exception {
        String sql =" update toursInfo set introduce =?,pubTime=?, price=?,cityId=? where id=?";
        Object[] paramterList=new Object[]{toursInfo.getIntroduce(),toursInfo.getPubTime(),toursInfo.getPrice(),toursInfo.getCityId(),toursInfo.getId()};
        int i=  qr.update(sql,paramterList);
        return i;
    }

    @Override
    public Integer getCount(Condition condition) throws Exception {

        //创建占位符的集合
        List<Object> parmsList =new ArrayList<>();
        String  cityId =condition.getCityId();
        String startPrice = condition.getStartPrice();
        String  endPrice = condition.getEndPrice();
        String  searchName =condition.getSearchName();

        String sql=" select count(1) from toursInfo t left join city c on t.cityId = c.cityId where 1=1 ";

        if(cityId!=null && !cityId.isEmpty()){
            sql+=" and t.cityId = ? ";
            parmsList.add(cityId);
        }

        if(startPrice!=null && !startPrice.isEmpty()){
            sql += " and t.price >= ? ";
            parmsList.add(startPrice);
        }
        if(endPrice!=null && !endPrice.isEmpty()){
            sql+= " and t.price <= ? ";
            parmsList.add(endPrice);
        }
        if(searchName!=null&&!searchName.isEmpty()){
            sql+= " and t.introduce like ? ";
            parmsList.add("%"+searchName+"%");
        }
        sql += " order by t.id ";

        Long log = qr.query(sql, new ScalarHandler<Long>(),parmsList.toArray());

        return log.intValue();

    }


    @Override
    public List<ToursInfo> queryToursInfoByPageBean(Condition condition, PageBean pb) throws Exception {

        List<Object> parmsList =new ArrayList<>();
        String  cityId =condition.getCityId();
        String startPrice = condition.getStartPrice();
        String  endPrice = condition.getEndPrice();
        String  searchName =condition.getSearchName();

        String sql=" select t.*, c.cityName from toursInfo t left join city c on t.cityId = c.cityId where 1=1 ";

        if(cityId!=null && !cityId.isEmpty()){
            sql+=" and t.cityId = ? ";
            parmsList.add(cityId);
        }
        if(startPrice!=null && !startPrice.isEmpty()){
            sql += " and t.price >= ? ";
            parmsList.add(startPrice);
        }
        if(endPrice!=null && !endPrice.isEmpty()){
            sql+= " and t.price <= ? ";
            parmsList.add(endPrice);
        }
        if(searchName!=null&&!searchName.isEmpty()){
            sql+= " and t.introduce like ? ";
            parmsList.add("%"+searchName+"%");
        }
        sql += " order by t.id ";
        sql +=" limit ?,? ";
        parmsList.add((pb.getP()-1)*pb.getPageSize());
        parmsList.add(pb.getPageSize());

        List<ToursInfo> toursInfolist = qr.query(sql, new BeanListHandler<ToursInfo>(ToursInfo.class), parmsList.toArray());

        return toursInfolist;
    }


}
