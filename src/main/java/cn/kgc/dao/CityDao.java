package cn.kgc.dao;

import cn.kgc.domain.City;

import java.util.List;

public interface CityDao  {
    public List<City> queryCity() throws Exception;

   /* public City addCity (City city);

    public City modifyCity (City city);*/
}
