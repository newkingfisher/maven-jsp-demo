package cn.kgc.dao;

import cn.kgc.Utils.PageBean;
import cn.kgc.domain.Condition;
import cn.kgc.domain.ToursInfo;

import java.util.List;

public interface ToursInfoDao {

    //按条件查询
    public List<ToursInfo> queryToursInfoByCondition(Condition condition)throws Exception;
    // 插入景点信息

    public Integer insertToursInfo(ToursInfo toursInfo)throws Exception;

    //删除景点信息
    public Integer deleteToursInfoById(String id) throws Exception;

    //修改景点信息
    public Integer updateToursInfo(ToursInfo toursInfo)throws Exception;
    // 通过Id查询景点信息
    public ToursInfo queryToursInfoById(String id) throws Exception;

    //查询记录数量
    public  Integer  getCount(Condition condition)throws Exception;

    public List<ToursInfo> queryToursInfoByPageBean(Condition condition, PageBean pb)throws Exception;

}
