package cn.kgc.servlet;

import cn.kgc.Utils.PageBean;
import cn.kgc.dao.CityDao;
import cn.kgc.domain.City;
import cn.kgc.domain.Condition;
import cn.kgc.domain.ToursInfo;
import cn.kgc.service.DemoService;
import cn.kgc.service.Impl.DemoServiceImpl;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@WebServlet(name = "MainServlet", urlPatterns = "/main")

public class MainServlet extends HttpServlet {

     DemoService demoService =new DemoServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
         //获取cmd查询，判断是什么类型的操作
       String cmd= request.getParameter("cmd");
       //查询功能
       if(cmd!=null&& cmd.equals("list")){
           list(request,response);
       }
       if(cmd!=null && cmd.equals("add")){
           add(request,response);
       }
       if(cmd!=null && cmd.equals("detail")){
           detail(request,response);
       }
       if(cmd!=null && cmd.equals("delete")){
           try {
               delete(request,response);
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
       if(cmd!=null && cmd.equals("updateUI")){
           updateUI(request,response);
       }
       if(cmd!=null && cmd.equals("update")){

           update(request,response);
       }

    }
    protected void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        try {
            //取， 获取页面的查询条件
            Map<String, String[]> parameterMap =request.getParameterMap();


            //第一次访问默认第一页
            Integer p=1;
            //获取当前页码
            String  strp =request.getParameter("p");
            if(strp!=null&& !strp.isEmpty()){
                p=Integer.valueOf(strp);
            }
            //查询实体条件
            Condition condition =new Condition();
            BeanUtils.populate(condition,parameterMap);
            //调  调用service中的方法
            // 城市信息
            List<City> cityList= demoService.queryCity();
            //查询景点信息列表
          // List<ToursInfo> toursInfoList= demoService.queryToursInfoByCondition(condition);  普通查询

            //分页查询
            PageBean pb = demoService.queryToursInfoByPB(condition,p);


            //存  数据存储到域对象
               request.getSession().setAttribute("cityList",cityList);
                //把PageBean放到域对象中，传递到jsp页面显示
              request.setAttribute("pb",pb);
          //查询条件回显示到main界面
          request.setAttribute("condition", condition);


          // 转   转发页面
           request.getRequestDispatcher("/main.jsp").forward(request,response);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //新增景点信息功能

    protected void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           //取  获取页面的数据
            Map<String, String[]> parameterMap =request.getParameterMap();
            ToursInfo toursInfo =new ToursInfo();

        try {
            DateConverter dateConverter =new DateConverter();
            dateConverter.setPatterns(new String[]{"yyyy-MM-dd","yyyy-MM-dd HH:mm:ss"});
            ConvertUtils.register(dateConverter, Date.class);
            BeanUtils.populate(toursInfo,parameterMap);

            //调  调用service层的方法
            boolean b = demoService.addToursInfo(toursInfo);

            //存  把信息放入域对象
            if(b){
                //如果添加成功，就在主界面添加信息，表达添加成功
                  request.setAttribute("insert","success");

            }else{
                //如果失败，在主界面添加信息，表达失败
                request.setAttribute("insert","fail");
            }
            //转发到住界面  (再次调用list,查询所有信息）

           // request.getRequestDispatcher("/main.jsp").forward(request,response);  没有内容

            list(request,response);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //更新景点信息

    // 点击修改后页面回显toursInfo
    protected void updateUI(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
       String id =  request.getParameter("id");

        try {
            // 调用服务层
            ToursInfo toursInfo = demoService.queryToursInfoById(id);
            //把数据存入域对象
            request.setAttribute("toursInfo",toursInfo);
            //转
            request.getRequestDispatcher("update.jsp").forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        //取 获取修改信息
        Map<String, String[]> parameterMap =request.getParameterMap();
        ToursInfo toursInfo =new ToursInfo();

        //转换时间格式
        DateConverter dateConverter =new DateConverter();
          dateConverter.setPatterns(new String[]{"yyyy-MM-dd","yyyy-MM-dd HH:mm:ss"});
          ConvertUtils.register(dateConverter,Date.class);


        try {
            // 把页面信息封装实体
            BeanUtils.populate(toursInfo,parameterMap);

            //调用服务层
            boolean b = demoService.updateToursInfo(toursInfo);
            //存 把信息存入域对象
            if(b){
                request.setAttribute("update","success");
            }else{
                request.setAttribute("update","fail");
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //转  修改景点之后再查询一次
        list(request,response);

    }


      protected void delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
            request.setCharacterEncoding("utf-8");
            response.setContentType("text/html;charset=utf-8");
             //获取数据
            String id =(String) request.getParameter("id");
            //调用service层方法
            boolean b = demoService.deleteToursInfoById(id);

            if(b){
                request.setAttribute("delete","success");

            }else{
                request.setAttribute("delete","fail");

            }
          list(request,response);

        }

    protected void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        //从页面取数据
        String id = request.getParameter("id");

        try {
            //调用service
            ToursInfo toursInfo = demoService.queryToursInfoById(id);
            City   cityList =new City();
            // 存在域对象
            request.setAttribute("toursInfo",toursInfo);
            request.getSession().setAttribute("cityList",cityList);
            // 转
            request.getRequestDispatcher("detail.jsp").forward(request,response);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }




}
