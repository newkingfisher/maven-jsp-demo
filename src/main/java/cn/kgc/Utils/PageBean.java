package cn.kgc.Utils;

import java.util.ArrayList;
import java.util.List;


public class PageBean {
    private Integer count;  //总共记录条数；
    private Integer pageSize;  //一页记录数量 需设定
    private Integer pageTotal;  //总共页数   需计算
    private Integer p;           //当前页数
    private List data;        //查询结果数据

    public PageBean() {
        //数据初始化
        pageSize =3;
        data =new ArrayList();//默认创建集合对象
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
        if(count%pageSize==0){
            pageTotal =count/pageSize;
        }else{
            pageTotal =count/pageSize +1;
        }
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Integer pageTotal) {
        this.pageTotal = pageTotal;
    }

    public Integer getP() {
        return p;
    }

    //如果是首页不能上一页，如果是尾页不能下一页
    public void setP(Integer p) {
        if(p<1){
            p=1;
        }else if(p>pageTotal){
            p=pageTotal;
        }
            this.p = p;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "count=" + count +
                ", pageSize=" + pageSize +
                ", pageTotal=" + pageTotal +
                ", p=" + p +
                ", data=" + data +
                '}';
    }
}
