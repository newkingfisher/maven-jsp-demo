<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/4/1
  Time: 4:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>修改景点信息</title>
</head>
<body>
<center>
    <h1>修改旅游景点</h1>
    <form action="${pageContext.request.contextPath}/main?cmd=update" method="post">
        <%--需要回显示记录的id--%>
        <input type="hidden" value="${toursInfo.id}" name="id">
        <table border="1" cellspacing="0" cellpadding="10" width="60%">
            <tr>
                <td>景点介绍</td>
                <td><textarea name="introduce" id="" cols="30" rows="10">${toursInfo.introduce}</textarea></td>
            </tr>
            <tr>
                <td>发布时间</td>
                <td><input type="date" name="pubTime" value="${toursInfo.pubTime}"></td>
            </tr>
            <tr>
                <td>票价</td>
                <td><input type="text" name="price" value="${toursInfo.price}">
                </td>
            </tr>
            <tr>
                <td>城市</td>
                <td>
                    <select name="cityId">
                        <c:forEach items="${cityList}" var="city">
                            <option value="${city.cityId}"
                                    <c:if test="${city.cityId==toursInfo.cityId}">selected</c:if>
                            >${city.cityName}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
        </table>
        <input type="submit" value="提交" >
    </form>
</center>

</body>
</html>
