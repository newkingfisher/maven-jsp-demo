<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/3/29
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="${pageContext.request.contextPath}/static/js/jquery-3.4.1.js"></script>
<html>
<head>
    <title>Title</title>
</head>

<body>
<center>
<h2>旅游景点信息列表</h2>
<form action="${pageContext.request.contextPath}/main?cmd=list" method="post" name="form1">
    <%--隐藏域：当前是第几页--%>
    <input type="hidden" name="p" id="p">
城市名称：
    <!--//动态查询数据--><select name="cityId">
        <option value="">==全部==</option>
        <c:forEach items="${cityList}" var="city">
            <option value="${city.cityId}" <c:if test="${city.cityId==condition.cityId}">selected</c:if>
            >${city.cityName}</option>
        </c:forEach>
    </select>
        价格区间：<input type="text" name="startPrice" id="startPrice" value="${condition.startPrice}"> -
        <input type="text" name="endPrice" id="endPrice" value="${condition.endPrice}">
        景点介绍：
        <input type="text" name="searchName" id="searchName" value="${condition.searchName}">
        <input type="submit" value="查找" >
        <input type="button" value="增加景点信息" onclick="addToursInfo()">


</form>
    <%--显示景点信息--%>
<table border="1" cellpadding="10" cellspacing="0" width="80%">
    <tr>
        <th>景点编号</th>
        <th>景点介绍</th>
        <th>发布时间</th>
        <th>票价</th>
        <th>城市名称</th>
        <th>删除</th>
        <th>详细</th>
        <th>修改</th>
    </tr>
    <!--//动态查询数据-->
    <c:forEach items="${pb.data}" var="tour" varStatus="vs">
    <tr <c:if test="${vs.index%2==0}">bgcolor="aqua" </c:if> >
        <td>${tour.id}</td>
        <td>${tour.introduce}</td>
        <td>${tour.pubTime}</td>
        <td>${tour.price}</td>
        <td>${tour.cityName}</td>
        <%--点击超链接 就跳转到js函数判断是否删除--%>
            <%--语法：javacript 函数名（）--%>
        <td><a href="javascript:del(${tour.id})">删除</a></td>
        <td><a href="javascript:detail(${tour.id})">详细</a></td>
        <td><a href="javascript:updateUI(${tour.id})">修改</a></td>
    </tr>
    </c:forEach>

</table>
    <p>
        <%--分页的数据展示--%>
        共${pb.count}条记录，第${pb.p}页/共${pb.pageTotal}页&nbsp;
        <a href="javascript:toPage(1)">首页</a>&nbsp;
        <a href="javascript:toPage(${pb.p-1})">上一页</a>&nbsp;
        <a href="javascript:toPage(${pb.p+1})">下一页</a>&nbsp;
        <a href="javascript:toPage(${pb.pageTotal})">尾页</a>&nbsp;
           <%--选择相应页数就跳转该页--%>
        跳转到第<select >
          <c:forEach var="i" begin="1" end="${pb.pageTotal}" step="1">
              <option >${i}</option>
          </c:forEach>
        </select>页，到第<input type="text" size="5">页，点击<input type="button" value="跳转">

    </p>
</center>
<script>
    function addToursInfo() {
        window.location.href="${pageContext.request.contextPath}/addToursInfo.jsp";

    }
    function del(id) {
        if(confirm("是否删除？")){
            window.location.href="${pageContext.request.contextPath}/main?cmd=delete&id="+id;
        }
    }
        /*分页查询*/
        function toPage(p) {
            //获取隐藏域p标签的值

            $("#p").val(p);
            //提交表单
            //1,给form加属性
            //2,通过document.name属性
            document.form1.submit();
    }
    function updateUI(id) {

            window.location.href="${pageContext.request.contextPath}/main?cmd=updateUI&id="+id;

    }
    function detail(id) {

        window.location.href="${pageContext.request.contextPath}/main?cmd=detail&id="+id;

    }
</script>

<script>
    <c:if test="${insert=='success'}">
       alert("插入成功")
    </c:if>
    <c:if test="${insert=='fail'}">
    alert("插入失败")
    </c:if>
    <c:if test="${update=='success'}">
      alert("修改成功")
    </c:if>
    <c:if test="${update=='fail'}">
     alert("修改失败")
    </c:if>
</script>
</body>
</html>
