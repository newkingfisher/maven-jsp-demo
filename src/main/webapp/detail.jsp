<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/4/1
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>详细信息</title>
</head>
<body>
<center>
    <form  action="${pageContext.request.contextPath}/main?cmd=list" method="post">
        <%--需要回显示记录的id--%>
        <input type="hidden" name="id" value="${toursInfo.id}">
        <table border="1" cellpadding="10" cellspacing="0" width="60%">
            <tr>
                <td>景点介绍</td>
                <td><textarea name="introduce"  id="" cols="30" rows="10">${toursInfo.introduce} </textarea>
                </td>
            </tr>
            <tr>
                <td>发布时间</td>
                <td><input type="date" name="pubTime" value="${toursInfo.pubTime}"></td>
            </tr>
            <tr>
                <td>票价</td>
                <td><input type="text"  name ="price" >${toursInfo.price}</td>
            </tr>
             <tr>
                 <td>城市</td>
                 <td>
                     <select name="cityId">
                         <c:forEach items="${cityList}" var="city">
                             <option name="${city.cityId}"
                             <c:if test="${city.cityId==toursInfo.cityId}">selected </c:if>
                             ${city.cityName}</option>
                         </c:forEach>
                     </select>
                 </td>
             </tr>
        </table>
          <input type="submit" value="返回主页" name="tomain">

    </form>
</center>
<%--<script>
    function tomain() {
        window.location.href="${pageContext.request.contextPath}/main?cmd=list";
    }
</script>--%>

</body>
</html>
