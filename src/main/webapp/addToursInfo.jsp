<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/3/30
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>旅游景点增加</title>
</head>
<body>
<center>
<h1>增加旅游景点</h1>
<form action="${pageContext.request.contextPath}/main?cmd=add" method="post">
    <table border="1" cellspacing="0" cellpadding="10" width="60%">
        <tr>
            <td>景点介绍</td>
            <td><textarea name="introduce" id="" cols="30" rows="10"></textarea></td>
        </tr>
        <tr>
            <td>发布时间</td>
            <td><input type="date" name="pubTime"></td>
        </tr>
        <tr>
            <td>票价</td>
            <td><input type="text" name="price">
            </td>
        </tr>
        <tr>
            <td>城市</td>
            <td>
                <select name="cityId">
                    <c:forEach items="${cityList}" var="city">
                        <option value="${city.cityId}">${city.cityName}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
    </table>
    <input type="submit" value="提交" >
</form>
</center>
</body>
</html>
